package com.mrgnstrn.malfriends.service;

import java.time.LocalDateTime;

public interface TimeService {

    LocalDateTime now();
}
