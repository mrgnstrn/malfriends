package com.mrgnstrn.malfriends.service;

import com.mrgnstrn.malfriends.entity.AnimeList;

import java.util.List;
import java.util.Optional;

public interface AnimeListService {

    List<AnimeList> getAllAnimeLists();


    AnimeList getAnimeListForUser(String username);

    double calculateAverageCompletedScoreFor(AnimeList animeList);
}
