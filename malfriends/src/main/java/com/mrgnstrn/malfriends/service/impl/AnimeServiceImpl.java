package com.mrgnstrn.malfriends.service.impl;

import com.mrgnstrn.malfriends.dao.AnimeRepository;
import com.mrgnstrn.malfriends.entity.Anime;
import com.mrgnstrn.malfriends.service.AnimeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
public class AnimeServiceImpl implements AnimeService {

    @Resource
    private AnimeRepository animeRepository;

    @Override
    public Optional<Anime> getAnimeByMalId(String malId) {
        return Optional.ofNullable(animeRepository.findByMalId(malId));
    }
}
