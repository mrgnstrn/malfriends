package com.mrgnstrn.malfriends.service.impl;

import com.mrgnstrn.malfriends.dao.MalProfileRepository;
import com.mrgnstrn.malfriends.entity.MalProfile;
import com.mrgnstrn.malfriends.scrapping.parser.MalFriendsPageParser;
import com.mrgnstrn.malfriends.service.MalProfileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MalProfileServiceImpl implements MalProfileService {

    @Resource
    private MalFriendsPageParser malFriendsPageParser;
    @Resource
    private MalProfileRepository malProfileRepository;

    @Override
    public List<MalProfile> getAllProfiles() {
        return malProfileRepository.findAll();
    }
}
