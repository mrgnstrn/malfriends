package com.mrgnstrn.malfriends.service;

import com.mrgnstrn.malfriends.entity.AnimeList;

public interface SynchronizationService {

    void synchronizeMalProfile(String username);

    void synchronizeAnimeList(String username);

    void synchronizeAnime(AnimeList animeList);

    void synchronizeAnime(String malId);

    void synchronizeAnimeLastUpdateAware(String malId);

}
