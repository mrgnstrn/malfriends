package com.mrgnstrn.malfriends.service;

import com.mrgnstrn.malfriends.entity.Anime;

import java.util.Optional;

public interface AnimeService {
    Optional<Anime> getAnimeByMalId(String malId);
}
