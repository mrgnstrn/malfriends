package com.mrgnstrn.malfriends.service;

import com.mrgnstrn.malfriends.entity.MalProfile;

import java.util.List;

public interface MalProfileService {

    List<MalProfile> getAllProfiles();
}