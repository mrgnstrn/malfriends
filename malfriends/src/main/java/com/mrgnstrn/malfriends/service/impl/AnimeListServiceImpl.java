package com.mrgnstrn.malfriends.service.impl;

import com.mrgnstrn.malfriends.dao.AnimeListRepository;
import com.mrgnstrn.malfriends.entity.Anime;
import com.mrgnstrn.malfriends.entity.AnimeEntryStatus;
import com.mrgnstrn.malfriends.entity.AnimeList;
import com.mrgnstrn.malfriends.entity.AnimeListEntry;
import com.mrgnstrn.malfriends.service.AnimeListService;
import com.mrgnstrn.malfriends.service.AnimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class AnimeListServiceImpl implements AnimeListService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnimeListServiceImpl.class);

    @Resource
    private AnimeListRepository animeListRepository;
    @Resource
    private AnimeService animeService;

    @Override
    public List<AnimeList> getAllAnimeLists() {
        return animeListRepository.findAll();
    }

    @Override
    public AnimeList getAnimeListForUser(String username) {
        return animeListRepository.findByUsername(username);
    }

    @Override
    public double calculateAverageCompletedScoreFor(AnimeList animeList) {
        List<AnimeListEntry> animeListEntries = animeList.getAnimeListEntries();
        List<Anime> animes = new ArrayList<>();
        for (AnimeListEntry animeListEntry : animeListEntries) {
            if (animeListEntry.getStatus().equals(AnimeEntryStatus.COMPLETED)) {
                String malId = animeListEntry.getMalId();
                animeService.getAnimeByMalId(malId).ifPresent(animes::add);
            }
        }
        int count = animes.size();
        double accum = 0;
        for (Anime anime : animes) {
            Double score = anime.getScore();
            if (score != null) {
                accum += anime.getScore();
            } else {
                count--;
            }

        }
        LOGGER.info(MessageFormat.format("Calculated average completed mean for [{0}]. Calculated based on [{1}]", animeList.getUsername(), count));
        return accum / count;
    }
}
