package com.mrgnstrn.malfriends.service.impl;

import com.mrgnstrn.malfriends.service.TimeService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
@Component
public class TimeServiceImpl implements TimeService {
    @Override
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
