package com.mrgnstrn.malfriends.service.impl;

import com.mrgnstrn.malfriends.converter.Converter;
import com.mrgnstrn.malfriends.dao.AnimeListRepository;
import com.mrgnstrn.malfriends.dao.AnimeRepository;
import com.mrgnstrn.malfriends.dao.MalProfileRepository;
import com.mrgnstrn.malfriends.entity.Anime;
import com.mrgnstrn.malfriends.entity.AnimeList;
import com.mrgnstrn.malfriends.entity.AnimeListEntry;
import com.mrgnstrn.malfriends.entity.MalProfile;
import com.mrgnstrn.malfriends.scrapping.parser.MalAnimePageParser;
import com.mrgnstrn.malfriends.scrapping.parser.MalFriendsPageParser;
import com.mrgnstrn.malfriends.scrapping.parser.MalProfilePageParser;
import com.mrgnstrn.malfriends.scrapping.parser.page.MalAnimePage;
import com.mrgnstrn.malfriends.scrapping.parser.page.MalProfilePage;
import com.mrgnstrn.malfriends.scrapping.restclient.client.MyAnimeListRestClient;
import com.mrgnstrn.malfriends.scrapping.restclient.entity.AnimelistData;
import com.mrgnstrn.malfriends.service.SynchronizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.List;


@Service
public class SynchronizationServiceImpl implements SynchronizationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SynchronizationServiceImpl.class);
    @Resource
    private MalFriendsPageParser malFriendsPageParser;
    @Resource
    private MalProfilePageParser malProfilePageParser;
    @Resource
    private MalAnimePageParser malAnimePageParser;
    @Resource
    private MalProfileRepository malProfileRepository;
    @Resource
    private AnimeListRepository animeListRepository;
    @Resource
    private AnimeRepository animeRepository;
    @Resource
    private MyAnimeListRestClient myAnimeListRestClient;
    @Resource
    private Converter<MalProfilePage, MalProfile> malProfilePageConverter;
    @Resource
    private Converter<MalAnimePage, Anime> malAnimePageConverter;
    @Resource
    private Converter<AnimelistData, AnimeList> animeListDataConverter;

    @Override
    public void synchronizeMalProfile(String username) {
        MalProfilePage malProfilePage = malProfilePageParser.parse(username);
        MalProfile malProfile = malProfilePageConverter.convert(malProfilePage);
        MalProfile malProfileInDb = malProfileRepository.findByUsername(username);
        if (malProfileInDb != null) {
            malProfile.setId(malProfileInDb.getId());
        }
        malProfile.setLastUpdated(LocalDateTime.now());
        malProfileRepository.save(malProfile);

    }

    @Override
    public void synchronizeAnimeList(String username) {
        AnimelistData animeListData = myAnimeListRestClient.getAnimeListData(username);
        AnimeList animeList = animeListDataConverter.convert(animeListData);
        AnimeList listInDb = animeListRepository.findByUsername(username);
        if (listInDb != null) {
            animeList.setId(listInDb.getId());
        }
        animeList.setLastUpdated(LocalDateTime.now());
        animeListRepository.save(animeList);

    }

    @Override
    public void synchronizeAnime(AnimeList animeList) {
        LOGGER.info(MessageFormat.format("Synchronization of anime list for [{0}] started", animeList.getUsername()));
        List<AnimeListEntry> animeListEntries = animeList.getAnimeListEntries();
        for (AnimeListEntry animeListEntry : animeListEntries) {
            synchronizeAnime(animeListEntry.getMalId());
        }

    }

    @Override
    public void synchronizeAnime(String malId) {
        LOGGER.info(MessageFormat.format("Synchronization of anime with id [{0}] started", malId));
        Anime anime = animeRepository.findByMalId(malId);
        searchAndSave(malId, anime);
    }

    @Override
    public void synchronizeAnimeLastUpdateAware(String malId) {
        LOGGER.info(MessageFormat.format("Synchronization of anime last update aware with id [{0}] started", malId));
        Anime anime = animeRepository.findByMalId(malId);
        if (anime != null) {
            LocalDateTime lastUpdated = anime.getLastUpdated();
            if (lastUpdated.isBefore(LocalDateTime.now().minusDays(1))) {
                searchAndSave(malId, anime);
            }
        } else {
            searchAndSave(malId, null);
        }
    }

    private void searchAndSave(String malId, Anime animeInDb) {
        MalAnimePage malAnimePage = malAnimePageParser.parse(malId);
        Anime anime = malAnimePageConverter.convert(malAnimePage);
        anime.setLastUpdated(LocalDateTime.now());
        if (animeInDb != null) {
            anime.setId(animeInDb.getId());
        }
        animeRepository.save(anime);
    }
}
