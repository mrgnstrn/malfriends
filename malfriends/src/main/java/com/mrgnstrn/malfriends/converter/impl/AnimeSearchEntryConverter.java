package com.mrgnstrn.malfriends.converter.impl;

import com.mrgnstrn.malfriends.converter.Converter;
import com.mrgnstrn.malfriends.entity.Anime;
import com.mrgnstrn.malfriends.entity.AnimeStatus;
import com.mrgnstrn.malfriends.entity.AnimeType;
import com.mrgnstrn.malfriends.scrapping.restclient.entity.AnimeSearchEntry;
import org.springframework.stereotype.Component;


@Component
public class AnimeSearchEntryConverter implements Converter<AnimeSearchEntry, Anime> {
    @Override
    public Anime convert(AnimeSearchEntry animeSearchEntry) {
        Anime anime = new Anime();
        anime.setEpisodes(animeSearchEntry.getEpisodes());
        anime.setStatus(AnimeStatus.fromStatus(animeSearchEntry.getStatus()));
        anime.setImageurl(animeSearchEntry.getImageUrl());
        anime.setTitle(animeSearchEntry.getTitle());
        anime.setScore(animeSearchEntry.getScore());
        anime.setMalId(animeSearchEntry.getId());
        anime.setType(AnimeType.fromType(animeSearchEntry.getType()));
        return anime;
    }
}
