package com.mrgnstrn.malfriends.converter.impl;

import com.mrgnstrn.malfriends.converter.Converter;
import com.mrgnstrn.malfriends.entity.AnimeList;
import com.mrgnstrn.malfriends.entity.MalProfile;
import com.mrgnstrn.malfriends.service.AnimeListService;
import com.mrgnstrn.malfriends.web.dto.MalProfileData;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.DecimalFormat;

@Component
public class MalProfileConverter implements Converter<MalProfile, MalProfileData> {

    @Resource
    private AnimeListService animeListService;

    @Override
    public MalProfileData convert(MalProfile malProfile) {
        MalProfileData malProfileData = new MalProfileData();
        malProfileData.setCompleted(malProfile.getCompleted());
        malProfileData.setUsername(malProfile.getUsername());
        malProfileData.setDays(malProfile.getDays());
        malProfileData.setDaysToCompletedRatio(malProfile.getDays() / malProfile.getCompleted());
        malProfileData.setMalAvatarurl(malProfile.getAvatarUrl());
        malProfileData.setMeanScore(malProfile.getMeansScore());
        populateMeanViewedScore(malProfile, malProfileData);
        return malProfileData;
    }

    private void populateMeanViewedScore(MalProfile malProfile, MalProfileData malProfileData) {

        AnimeList animeList = animeListService.getAnimeListForUser(malProfile.getUsername());
        if (animeList != null) {
            double meanViewedScore = animeListService.calculateAverageCompletedScoreFor(animeList);
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            malProfileData.setMeanViewedScore(Double.valueOf(decimalFormat.format(meanViewedScore)));
        }
    }
}

