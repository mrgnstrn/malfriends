package com.mrgnstrn.malfriends.converter.impl;

import com.mrgnstrn.malfriends.converter.Converter;
import com.mrgnstrn.malfriends.entity.AnimeEntryStatus;
import com.mrgnstrn.malfriends.entity.AnimeListEntry;
import com.mrgnstrn.malfriends.scrapping.restclient.entity.AnimeData;
import org.springframework.stereotype.Component;

@Component
public class AnimeDataConverter implements Converter<AnimeData, AnimeListEntry> {
    @Override
    public AnimeListEntry convert(AnimeData animeData) {
        AnimeListEntry animeListEntry = new AnimeListEntry();
        animeListEntry.setMalId(animeData.getId());
        animeListEntry.setTitle(animeData.getTitle());
        animeListEntry.setStatus(AnimeEntryStatus.fromStatusCode(animeData.getStatus()));
        return animeListEntry;
    }
}
