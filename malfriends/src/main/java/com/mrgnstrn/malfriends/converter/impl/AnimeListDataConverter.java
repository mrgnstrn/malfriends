package com.mrgnstrn.malfriends.converter.impl;

import com.mrgnstrn.malfriends.converter.Converter;
import com.mrgnstrn.malfriends.entity.AnimeList;
import com.mrgnstrn.malfriends.entity.AnimeListEntry;
import com.mrgnstrn.malfriends.scrapping.restclient.entity.AnimeData;
import com.mrgnstrn.malfriends.scrapping.restclient.entity.AnimelistData;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AnimeListDataConverter implements Converter<AnimelistData, AnimeList> {

    @Resource
    private Converter<AnimeData, AnimeListEntry> animeDataConverter;

    @Override
    public AnimeList convert(AnimelistData animeData) {
        AnimeList animeList = new AnimeList();
        animeList.setUsername(animeData.getMyInfoData().getUsername());
        List<AnimeData> animes = animeData.getAnime();
        List<AnimeListEntry> animeListEntries = animes.stream().map(anime -> animeDataConverter.convert(anime)).collect(Collectors.toList());
        animeList.setAnimeListEntries(animeListEntries);
        return animeList;
    }
}
