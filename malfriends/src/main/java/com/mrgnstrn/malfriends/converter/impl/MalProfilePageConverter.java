package com.mrgnstrn.malfriends.converter.impl;

import com.mrgnstrn.malfriends.converter.Converter;
import com.mrgnstrn.malfriends.entity.MalProfile;
import com.mrgnstrn.malfriends.scrapping.parser.page.MalProfilePage;
import org.springframework.stereotype.Component;

@Component
public class MalProfilePageConverter implements Converter<MalProfilePage, MalProfile> {
    @Override
    public MalProfile convert(MalProfilePage malProfilePage) {
        MalProfile malProfile = new MalProfile();
        malProfile.setUsername(malProfilePage.getUsername());
        malProfile.setAvatarUrl(malProfilePage.getAvatarUrl());
        malProfile.setDays(malProfilePage.getDays());
        malProfile.setMeansScore(malProfilePage.getMeansScore());
        malProfile.setWatching(malProfilePage.getWatching());
        malProfile.setCompleted(malProfilePage.getCompleted());
        malProfile.setOnHold(malProfilePage.getOnHold());
        malProfile.setDropped(malProfilePage.getDropped());
        malProfile.setPlanToWatch(malProfilePage.getPlanToWatch());
        return malProfile;
    }
}
