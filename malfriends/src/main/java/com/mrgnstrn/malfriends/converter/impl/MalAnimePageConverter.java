package com.mrgnstrn.malfriends.converter.impl;

import com.mrgnstrn.malfriends.converter.Converter;
import com.mrgnstrn.malfriends.entity.Anime;
import com.mrgnstrn.malfriends.entity.AnimeStatus;
import com.mrgnstrn.malfriends.entity.AnimeType;
import com.mrgnstrn.malfriends.scrapping.parser.page.MalAnimePage;
import org.springframework.stereotype.Component;

@Component
public class MalAnimePageConverter implements Converter<MalAnimePage, Anime> {
    @Override
    public Anime convert(MalAnimePage malAnimePage) {
        Anime anime = new Anime();
        anime.setMalId(malAnimePage.getMalId());
        anime.setType(AnimeType.fromType(malAnimePage.getType()));
        anime.setScore(malAnimePage.getScore());
        anime.setTitle(malAnimePage.getTitle());
        anime.setImageurl(malAnimePage.getImageUrl());
        anime.setStatus(AnimeStatus.fromStatus(malAnimePage.getStatus()));
        anime.setEpisodes(malAnimePage.getEpisodes());
        return anime;
    }
}
