package com.mrgnstrn.malfriends.converter;

public interface Converter<SOURCE, TARGET> {

    TARGET convert(SOURCE source);
}
