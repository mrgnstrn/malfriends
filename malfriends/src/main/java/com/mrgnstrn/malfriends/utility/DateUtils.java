package com.mrgnstrn.malfriends.utility;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class DateUtils {

    private static final String SEPARATOR = "-";

    public static LocalDate parseDate(String dateString) {

        String[] splittedDate = dateString.split(SEPARATOR);
        String year = splittedDate[0];
        String month = splittedDate[1];
        String day = splittedDate[2];

        if (day.equalsIgnoreCase("00")) {
            day = "01";
        }
        if (month.equalsIgnoreCase("00")) {
            month = "01";
        }
        String assembledDate = year + SEPARATOR + month + SEPARATOR + day;
        try {
            return LocalDate.parse(assembledDate);
        } catch (DateTimeParseException ex) {
            return null;
        }
    }
}
