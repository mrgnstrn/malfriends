package com.mrgnstrn.malfriends;

import com.mrgnstrn.malfriends.cronjobs.SynchronizeMalProfileDataJob;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.Resource;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableRetry
public class MalfriendsApplication  {

    @Resource
    private SynchronizeMalProfileDataJob synchronizeMalProfileDataJob;

    public static void main(String[] args) {
        SpringApplication.run(MalfriendsApplication.class, args);
    }

}
