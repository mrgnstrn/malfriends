package com.mrgnstrn.malfriends.security;

import com.mrgnstrn.malfriends.dao.UserRepository;
import com.mrgnstrn.malfriends.entity.User;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class MongoUserDetailService implements UserDetailsService {

    @Resource
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User foundUser = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(""));
        return new org.springframework.security.core.userdetails.User(foundUser.getUsername(), foundUser.getPassword(), AuthorityUtils.createAuthorityList(foundUser.getRole().toString()));
    }
}
