package com.mrgnstrn.malfriends.web.controller;

import com.mrgnstrn.malfriends.service.MalProfileService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

@Controller
public class IndexPageController {

    @Resource
    private MalProfileService malProfileService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }


}
