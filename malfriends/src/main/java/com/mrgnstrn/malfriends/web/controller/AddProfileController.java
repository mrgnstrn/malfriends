package com.mrgnstrn.malfriends.web.controller;

import com.mrgnstrn.malfriends.service.SynchronizationService;
import com.mrgnstrn.malfriends.web.forms.AddMalProfileForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.validation.Valid;

@Controller
public class AddProfileController {
    @Resource
    private SynchronizationService synchronizationService;

    @RequestMapping(value = "/addMalProfile", method = RequestMethod.GET)
    public String getPage(Model model) {
        return "addProfile";
    }

    @RequestMapping(value = "/addMalProfile", method = RequestMethod.POST)
    public String addMalProfile(@Valid AddMalProfileForm addMalProfileForm, Model model) {
        synchronizationService.synchronizeMalProfile(addMalProfileForm.getUsername());
        return "redirect:/rating";
    }
}
