package com.mrgnstrn.malfriends.web.controller;

import com.mrgnstrn.malfriends.cronjobs.SynchronizeAnimeJob;
import com.mrgnstrn.malfriends.service.MalProfileService;
import com.mrgnstrn.malfriends.service.SynchronizationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
public class ControlPanelController {

    @Resource
    private SynchronizeAnimeJob synchronizeAnimeJob;
    @Resource
    private MalProfileService malProfileService;
    @Resource
    private SynchronizationService synchronizationService;

    @RequestMapping(value = "/controlPanel", method = RequestMethod.GET)
    public String getGlobalRating() {
        return "controlPanel";
    }

    @RequestMapping(value = "/controlPanel/action", method = RequestMethod.GET)
    public String doAction(@RequestParam("id") int id) {

        switch (id) {
            case 1: {
                synchronizeAnimeJob.updateAnimeData();
                break;
            }
            case 2: {
                malProfileService.getAllProfiles().forEach(malProfile -> synchronizationService.synchronizeAnimeList(malProfile.getUsername()));
                break;
            }
        }

        return "controlPanel";
    }
}
