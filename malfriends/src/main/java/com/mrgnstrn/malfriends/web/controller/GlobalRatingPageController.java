package com.mrgnstrn.malfriends.web.controller;

import com.mrgnstrn.malfriends.converter.Converter;
import com.mrgnstrn.malfriends.entity.MalProfile;
import com.mrgnstrn.malfriends.service.MalProfileService;
import com.mrgnstrn.malfriends.web.dto.MalProfileData;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@Controller
public class GlobalRatingPageController {
    @Resource
    private MalProfileService malProfileService;
    @Resource
    private Converter<MalProfile, MalProfileData> malProfileConverter;

    @RequestMapping(value = "/rating", method = RequestMethod.GET)
    public String getGlobalRating(Model model) {
        List<MalProfile> allProfiles = malProfileService.getAllProfiles();
        List<MalProfileData> allProfilesData = new ArrayList<>();
        for (MalProfile allProfile : allProfiles) {
            MalProfileData malProfileData = malProfileConverter.convert(allProfile);
            allProfilesData.add(malProfileData);
        }
        model.addAttribute("profiles", allProfilesData);
        return "globalRating";
    }
}
