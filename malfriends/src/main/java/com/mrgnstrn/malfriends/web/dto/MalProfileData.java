package com.mrgnstrn.malfriends.web.dto;

public class MalProfileData {
    private String username;
    private String displayName;
    private double days;
    private int completed;
    private double daysToCompletedRatio;
    private String malAvatarurl;
    private double meanScore;
    private double meanViewedScore;

    public double getMeanViewedScore() {
        return meanViewedScore;
    }

    public void setMeanViewedScore(double meanViewedScore) {
        this.meanViewedScore = meanViewedScore;
    }

    public double getMeanScore() {
        return meanScore;
    }

    public void setMeanScore(double meanScore) {
        this.meanScore = meanScore;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public double getDays() {
        return days;
    }

    public void setDays(double days) {
        this.days = days;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public double getDaysToCompletedRatio() {
        return daysToCompletedRatio;
    }

    public void setDaysToCompletedRatio(double daysToCompletedRatio) {
        this.daysToCompletedRatio = daysToCompletedRatio;
    }

    public String getMalAvatarurl() {
        return malAvatarurl;
    }

    public void setMalAvatarurl(String malAvatarurl) {
        this.malAvatarurl = malAvatarurl;
    }
}
