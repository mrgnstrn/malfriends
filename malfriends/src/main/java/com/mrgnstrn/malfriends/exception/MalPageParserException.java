package com.mrgnstrn.malfriends.exception;

public class MalPageParserException extends RuntimeException {
    public MalPageParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
