package com.mrgnstrn.malfriends.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document
public class AnimeList {
    @Id
    private ObjectId id;
    @Indexed(unique = true)
    private String username;
    private List<AnimeListEntry> animeListEntries;
    private LocalDateTime lastUpdated;

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<AnimeListEntry> getAnimeListEntries() {
        return animeListEntries;
    }

    public void setAnimeListEntries(List<AnimeListEntry> animeListEntries) {
        this.animeListEntries = animeListEntries;
    }

    @Override
    public String toString() {
        return "AnimeList{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", animeListEntries=" + animeListEntries +
                ", lastUpdated=" + lastUpdated +
                '}';
    }
}
