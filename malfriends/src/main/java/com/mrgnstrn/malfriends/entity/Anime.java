package com.mrgnstrn.malfriends.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
public class Anime {
    @Id
    private ObjectId id;
    @Indexed(unique = true)
    private String malId;
    private String title;
    private Integer episodes;
    private Double score;
    private AnimeType type;
    private AnimeStatus status;
    private String imageurl;
    private LocalDateTime lastUpdated;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getMalId() {
        return malId;
    }

    public void setMalId(String malId) {
        this.malId = malId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getEpisodes() {
        return episodes;
    }

    public void setEpisodes(Integer episodes) {
        this.episodes = episodes;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public AnimeType getType() {
        return type;
    }

    public void setType(AnimeType type) {
        this.type = type;
    }

    public AnimeStatus getStatus() {
        return status;
    }

    public void setStatus(AnimeStatus status) {
        this.status = status;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "Anime{" +
                "id=" + id +
                ", malId='" + malId + '\'' +
                ", title='" + title + '\'' +
                ", episodes=" + episodes +
                ", score=" + score +
                ", type=" + type +
                ", status=" + status +
                ", imageurl='" + imageurl + '\'' +
                ", lastUpdated=" + lastUpdated +
                '}';
    }
}
