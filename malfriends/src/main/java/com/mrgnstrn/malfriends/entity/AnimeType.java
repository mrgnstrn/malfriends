package com.mrgnstrn.malfriends.entity;

public enum AnimeType {
    TV("TV"), MOVIE("Movie"), ONA("ONA"), OVA("OVA"), SPECIAL("Special"), MUSIC("Music");


    private final String type;

    AnimeType(String type) {
        this.type = type;
    }

    public static AnimeType fromType(String type) {
        for (AnimeType status : AnimeType.values()) {
            if (status.type.equalsIgnoreCase(type)) {
                return status;
            }
        }
        throw new IllegalArgumentException("No enum value for stsatus " + type);
    }
}
