package com.mrgnstrn.malfriends.entity;

public class AnimeListEntry {

    private String malId;
    private String title;
    private AnimeEntryStatus status;

    public String getMalId() {
        return malId;
    }

    public void setMalId(String malId) {
        this.malId = malId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AnimeEntryStatus getStatus() {
        return status;
    }

    public void setStatus(AnimeEntryStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AnimeListEntry{" +
                "malId='" + malId + '\'' +
                ", title='" + title + '\'' +
                ", status=" + status +
                '}';
    }
}
