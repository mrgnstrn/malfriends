package com.mrgnstrn.malfriends.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document
public class MalProfile {
    @Id
    private ObjectId id;
    @Indexed(unique = true)
    private String username;
    private String displayName;
    private String avatarUrl;
    private double days;
    private double meansScore;
    private int watching;
    private int completed;
    private int onHold;
    private int dropped;
    private int planToWatch;
    private LocalDateTime lastUpdated;
    private List<String> friendsUsernames;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<String> getFriendsUsernames() {
        return friendsUsernames;
    }

    public void setFriendsUsernames(List<String> friendsUsernames) {
        this.friendsUsernames = friendsUsernames;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public double getDays() {
        return days;
    }

    public void setDays(double days) {
        this.days = days;
    }

    public double getMeansScore() {
        return meansScore;
    }

    public void setMeansScore(double meansScore) {
        this.meansScore = meansScore;
    }

    public int getWatching() {
        return watching;
    }

    public void setWatching(int watching) {
        this.watching = watching;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public int getOnHold() {
        return onHold;
    }

    public void setOnHold(int onHold) {
        this.onHold = onHold;
    }

    public int getDropped() {
        return dropped;
    }

    public void setDropped(int dropped) {
        this.dropped = dropped;
    }

    public int getPlanToWatch() {
        return planToWatch;
    }

    public void setPlanToWatch(int planToWatch) {
        this.planToWatch = planToWatch;
    }

    @Override
    public String toString() {
        return "MalProfile{" +
                "username='" + username + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", days=" + days +
                ", meansScore=" + meansScore +
                ", watching=" + watching +
                ", completed=" + completed +
                ", onHold=" + onHold +
                ", dropped=" + dropped +
                ", planToWatch=" + planToWatch +
                '}';
    }
}
