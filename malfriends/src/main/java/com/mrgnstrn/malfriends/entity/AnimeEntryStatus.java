package com.mrgnstrn.malfriends.entity;

public enum AnimeEntryStatus {
    WATCHING("1"), COMPLETED("2"), PLANTOWATCH("6"), ONHOLD("3"), DROPPED("4");

    private final String statusCode;

    AnimeEntryStatus(String statusCode) {
        this.statusCode = statusCode;
    }

    public static AnimeEntryStatus fromStatusCode(String statusCode) {
        for (AnimeEntryStatus status : AnimeEntryStatus.values()) {
            if (status.statusCode.equalsIgnoreCase(statusCode)) {
                return status;
            }
        }
        throw new IllegalArgumentException("No enum value for stsatus " + statusCode);
    }
}
