package com.mrgnstrn.malfriends.entity;

public enum AnimeStatus {
    FINISHED("Finished Airing"), AIRING("Currently Airing"), NOTYETAIRED("Not yet aired");

    private final String status;

    AnimeStatus(String status) {
        this.status = status;
    }

    public static AnimeStatus fromStatus(String statusText) {
        for (AnimeStatus status : AnimeStatus.values()) {
            if (status.status.equalsIgnoreCase(statusText)) {
                return status;
            }
        }
        throw new IllegalArgumentException("No enum value for stsatus " + statusText);
    }
}
