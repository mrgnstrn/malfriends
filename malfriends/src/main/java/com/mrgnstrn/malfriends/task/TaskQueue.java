package com.mrgnstrn.malfriends.task;

import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

@Component
public class TaskQueue {

    private Queue<Runnable> taskQueue;

    public TaskQueue() {
        taskQueue = new LinkedBlockingQueue<>();
    }

    public void add(Runnable task) {
        taskQueue.add(task);
    }

    public Optional<Runnable> poll() {
        return Optional.ofNullable(taskQueue.poll());
    }

    public int size() {
        return taskQueue.size();
    }
}
