package com.mrgnstrn.malfriends.dao;

import com.mrgnstrn.malfriends.entity.Anime;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AnimeRepository extends MongoRepository<Anime, String> {

    Anime findByMalId(String malId);

}
