package com.mrgnstrn.malfriends.dao;

import com.mrgnstrn.malfriends.entity.AnimeList;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnimeListRepository extends MongoRepository<AnimeList, String> {

    AnimeList findByUsername(String username);
}
