package com.mrgnstrn.malfriends.dao;

import com.mrgnstrn.malfriends.entity.MalProfile;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MalProfileRepository extends MongoRepository<MalProfile, String> {
    MalProfile findByUsername(String username);

}
