package com.mrgnstrn.malfriends.cronjobs;

import com.mrgnstrn.malfriends.service.AnimeListService;
import com.mrgnstrn.malfriends.service.SynchronizationService;
import com.mrgnstrn.malfriends.task.TaskQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class SyncronizeAnimeListsJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(SyncronizeAnimeListsJob.class);

    @Resource
    private AnimeListService animeListService;
    @Resource
    private SynchronizationService synchronizationService;
    @Resource
    private TaskQueue taskQueue;

    @Scheduled(cron = "0 0 0/1 * * ?")
    public void updateAnimeLists() {
        LOGGER.info("Synchronize of anime lists started");
        animeListService.getAllAnimeLists().forEach(animeList -> taskQueue.add(() -> synchronizationService.synchronizeAnimeList(animeList.getUsername())));
        LOGGER.info("Synchronize of anime lists ended");
    }

}
