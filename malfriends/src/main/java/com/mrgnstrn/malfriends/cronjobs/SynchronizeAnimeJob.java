package com.mrgnstrn.malfriends.cronjobs;

import com.mrgnstrn.malfriends.entity.Anime;
import com.mrgnstrn.malfriends.entity.AnimeList;
import com.mrgnstrn.malfriends.entity.AnimeListEntry;
import com.mrgnstrn.malfriends.service.AnimeListService;
import com.mrgnstrn.malfriends.service.AnimeService;
import com.mrgnstrn.malfriends.service.SynchronizationService;
import com.mrgnstrn.malfriends.task.TaskQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
public class SynchronizeAnimeJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(SynchronizeAnimeJob.class);

    @Resource
    private AnimeListService animeListService;
    @Resource
    private AnimeService animeService;
    @Resource
    private SynchronizationService synchronizationService;
    @Resource
    private TaskQueue taskQueue;

    @Scheduled(cron = "0 0 0/12 * * ?")
    public void updateAnimeData() {
        LOGGER.info("Synchronize of anime started");
        List<AnimeList> allAnimeLists = animeListService.getAllAnimeLists();
        for (AnimeList animeList : allAnimeLists) {
            List<AnimeListEntry> animeListEntries = animeList.getAnimeListEntries();
            for (AnimeListEntry animeListEntry : animeListEntries) {
                Optional<Anime> animeByMalId = animeService.getAnimeByMalId(animeListEntry.getMalId());
                if (animeByMalId.isPresent()) {
                    Anime anime = animeByMalId.get();
                    LocalDateTime lastUpdated = anime.getLastUpdated();
                    if (lastUpdated.isBefore(LocalDateTime.now().minusDays(1))) {
                        sync(anime.getMalId());
                    }
                } else {
                    sync(animeListEntry.getMalId());
                }
            }
        }
        LOGGER.info("Synchronize of anime ended");
    }

    private void sync(String malId) {
        taskQueue.add(() -> synchronizationService.synchronizeAnimeLastUpdateAware(malId));
    }

}
