package com.mrgnstrn.malfriends.cronjobs;

import com.mrgnstrn.malfriends.entity.MalProfile;
import com.mrgnstrn.malfriends.service.MalProfileService;
import com.mrgnstrn.malfriends.service.SynchronizationService;
import com.mrgnstrn.malfriends.service.TimeService;
import com.mrgnstrn.malfriends.task.TaskQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class SynchronizeMalProfileDataJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(SynchronizeMalProfileDataJob.class);

    @Resource
    private MalProfileService malProfileService;
    @Resource
    private SynchronizationService synchronizationService;
    @Resource
    private TimeService timeService;
    @Resource
    private TaskQueue taskQueue;

    @Scheduled(cron = "0 0/10 * * * ?")
    public void updateProfilesData() {
        LOGGER.info("Synchronize of profiles started");
        List<MalProfile> allProfiles = malProfileService.getAllProfiles();
        LocalDateTime now = timeService.now();
        allProfiles.stream().filter(profile -> profile.getLastUpdated().isBefore(now.minusHours(1))).forEach(malProfile -> taskQueue.add(() -> synchronizationService.synchronizeMalProfile(malProfile.getUsername())));
        LOGGER.info("Synchronize of profiles ended");
    }
}
