package com.mrgnstrn.malfriends.cronjobs;

import com.mrgnstrn.malfriends.task.TaskQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.MessageFormat;

@Component
public class MyAnimeListRequestingJob {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyAnimeListRequestingJob.class);
    @Resource
    private TaskQueue taskQueue;
    @Resource
    private RetryTemplate retryTemplate;

    @Scheduled(fixedDelay = 1 * 1000)
    public void run() {
        int size = taskQueue.size();
        if (size > 0) {
            LOGGER.info(MessageFormat.format("[{0}] tasks left", size));
        }
        taskQueue.poll().ifPresent(task -> {
            retryTemplate.execute((RetryCallback<Void, RuntimeException>) context -> {
                task.run();
                return null;
            });
        });
    }

}
