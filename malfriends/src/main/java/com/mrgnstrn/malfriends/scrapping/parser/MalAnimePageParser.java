package com.mrgnstrn.malfriends.scrapping.parser;

import com.mrgnstrn.malfriends.exception.MalPageParserException;
import com.mrgnstrn.malfriends.scrapping.parser.page.MalAnimePage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.MessageFormat;

@Service
public class MalAnimePageParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(MalAnimePageParser.class);

    private static final String MAL_ANIME_PAGE = "https://myanimelist.net/anime/";

    public MalAnimePage parse(String malId) {
        try {

            MalAnimePage malAnimePage = new MalAnimePage();
            malAnimePage.setMalId(malId);
            Document document = Jsoup.connect(MAL_ANIME_PAGE + malId).get();
            malAnimePage.setTitle(extractTitle(document));
            malAnimePage.setScore(extractScore(document));
            malAnimePage.setUsersCountThatRated(extractUsersRated(document));
            malAnimePage.setEpisodes(extractEpisodes(document));
            malAnimePage.setType(extractType(document));
            malAnimePage.setStatus(extractStatus(document));
//            malAnimePage.setImageUrl(extractImageUrl(document));
//            malAnimePage.setPopularity(extractPopularity(document));
//            malAnimePage.setRanked(extractRanked(document));
            return malAnimePage;

        } catch (IOException e) {
            LOGGER.warn(MessageFormat.format("Cannot parse page for anime [{0}]", malId));
            throw new MalPageParserException("Cannot parse page", e);
        }
    }

//    private int extractRanked(Document document) {
//        return 0;
//    }
//
//    private int extractPopularity(Document document) {
//        return 0;
//    }
//
//    private String extractImageUrl(Document document) {
//        return null;
//    }

    private String extractStatus(Document document) {
        Elements spaceItDivs = getSpaceItDivs(document);
        Element element = spaceItDivs.get(6);
        Element next = element.nextElementSibling();
        next.child(0).remove();
        return next.text().trim();
    }

    private String extractType(Document document) {
        Elements spaceItDivs = getSpaceItDivs(document);
        Element element = spaceItDivs.get(6);
        Element prev = element.previousElementSibling();
        prev.child(0).remove();
        return prev.text().trim();
    }

    private Integer extractEpisodes(Document document) {
        Elements spaceItDivs = getSpaceItDivs(document);
        Element element = spaceItDivs.get(6);
        element.child(0).remove();
        String episodes = element.text().trim();
        if (episodes.equalsIgnoreCase("Unknown")) {
            return null;
        } else {
            return Integer.parseInt(episodes);
        }
    }

    private String extractTitle(Document document) {
        Element header = document.getElementsByClass("h1").get(0);
        return header.text().trim();
    }

    private Double extractScore(Document document) {
        Element scoreDiv = document.getElementsByClass("fl-l score").get(0);
        String trimScoreString = scoreDiv.text().trim();
        if (trimScoreString.equalsIgnoreCase("N/A")) {
            return null;
        } else {
            return Double.parseDouble(trimScoreString);
        }
    }

    private Integer extractUsersRated(Document document) {
        Element scoreDiv = document.getElementsByClass("fl-l score").get(0);
        String score = scoreDiv.attr("data-user");
        score = score.replaceAll("[^\\d]", "").trim();
        if (score.equalsIgnoreCase("N/A")) {
            return null;
        } else {
            return Integer.parseInt(score);
        }
    }

    private Elements getSpaceItDivs(Document document) {
        return document.getElementsByClass("spaceit");
    }
}
