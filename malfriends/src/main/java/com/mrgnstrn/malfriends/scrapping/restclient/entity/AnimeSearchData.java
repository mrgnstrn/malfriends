package com.mrgnstrn.malfriends.scrapping.restclient.entity;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "anime")
public class AnimeSearchData {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "entry")
    private List<AnimeSearchEntry> animeSearchEntries;

    public List<AnimeSearchEntry> getAnimeSearchEntries() {
        return animeSearchEntries;
    }

    public void setAnimeSearchEntries(List<AnimeSearchEntry> animeSearchEntries) {
        this.animeSearchEntries = animeSearchEntries;
    }

    @Override
    public String toString() {
        return "AnimeSearchData{" +
                "animeSearchEntries=" + animeSearchEntries +
                '}';
    }
}
