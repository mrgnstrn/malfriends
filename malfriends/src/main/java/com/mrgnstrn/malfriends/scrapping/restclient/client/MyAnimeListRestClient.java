package com.mrgnstrn.malfriends.scrapping.restclient.client;


import com.mrgnstrn.malfriends.scrapping.restclient.entity.AnimelistData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class MyAnimeListRestClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyAnimeListRestClient.class);

    public AnimelistData getAnimeListData(String username) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("https://myanimelist.net/malappinfo.php?u=" + username + "&type=anime&status=all", AnimelistData.class);
    }
}
