package com.mrgnstrn.malfriends.scrapping.parser.page;

import java.util.List;

public class MalFriendsPage {

    private List<String> friendsUsernames;

    public List<String> getFriendsUsernames() {
        return friendsUsernames;
    }

    public void setFriendsUsernames(List<String> friendsUsernames) {
        this.friendsUsernames = friendsUsernames;
    }

    @Override
    public String toString() {
        return "MalFriendsPage{" +
                "friendsUsernames=" + friendsUsernames +
                '}';
    }
}
