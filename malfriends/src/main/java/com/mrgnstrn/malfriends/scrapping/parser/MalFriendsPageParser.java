package com.mrgnstrn.malfriends.scrapping.parser;

import com.mrgnstrn.malfriends.exception.MalPageParserException;
import com.mrgnstrn.malfriends.scrapping.parser.page.MalFriendsPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MalFriendsPageParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(MalFriendsPageParser.class);
    private static final String MAL_PROFILE_URL = "https://myanimelist.net/profile/";
    private static final String MAL_FRIENDS_SUFFIX = "/friends";

    public MalFriendsPage parse(String username) {
        try {
            MalFriendsPage page = new MalFriendsPage();
            Document friendsPage = Jsoup.connect(MAL_PROFILE_URL + username + MAL_FRIENDS_SUFFIX).get();
            Elements friendsDivs = friendsPage.getElementsByClass("friendHolder");
            List<String> friendsUsernames = friendsDivs.stream().map(element -> element.getElementsByTag("strong")).map(elements -> elements.get(0)).map(Element::text).collect(Collectors.toList());
            page.setFriendsUsernames(friendsUsernames);
            return page;
        } catch (IOException e) {
            LOGGER.warn(MessageFormat.format("Cannot parse page for username [{0}]", username));
            throw new MalPageParserException("Cannot parse page", e);
        }
    }
}
