package com.mrgnstrn.malfriends.scrapping.parser;

import com.mrgnstrn.malfriends.exception.MalPageParserException;
import com.mrgnstrn.malfriends.scrapping.parser.page.MalProfilePage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.MessageFormat;

@Service
public class MalProfilePageParser {
    private static final String MAL_PROFILE_URL = "https://myanimelist.net/profile/";

    private static final int ANIME_DETAIL_STATS_INDEX_WATCHING = 0;
    private static final int ANIME_DETAIL_STATS_INDEX_COMPLETED = 1;
    private static final int ANIME_DETAIL_STATS_INDEX_ONHOLD = 2;
    private static final int ANIME_DETAIL_STATS_INDEX_DROPPED = 3;
    private static final int ANIME_DETAIL_STATS_INDEX_PLANTOWATCH = 4;


    public MalProfilePage parse(String username) {
        try {
            MalProfilePage page = new MalProfilePage();
            Document profilePage = Jsoup.connect(MAL_PROFILE_URL + username).get();
            page.setUsername(extractDisplayName(profilePage));
            page.setAvatarUrl(extractProfilePicture(profilePage));
            page.setDays(extractTotalDays(profilePage));
            page.setMeansScore(extractMeanScore(profilePage));
            page.setWatching(extractWatching(profilePage));
            page.setCompleted(extractCompleted(profilePage));
            page.setOnHold(extractOnhold(profilePage));
            page.setDropped(extractDropped(profilePage));
            page.setPlanToWatch(extractPlanToWatch(profilePage));
            return page;
        } catch (IOException e) {
            throw new MalPageParserException(MessageFormat.format("Cannot parse profile page for {0}", username), e);
        }
    }

    private String extractDisplayName(Document profilePage) {
        Element element = profilePage.getElementsByClass("di-ib po-r").get(0);
        String text = element.text().trim();
        return text.replace("'s Profile", "");
    }

    private String extractProfilePicture(Document profilePage) {
        Element userAvatarDiv = profilePage.getElementsByClass("user-image mb8").get(0);
        Element img = userAvatarDiv.getElementsByTag("img").first();
        return img == null ? null : img.attr("src");
    }

    private double extractTotalDays(Document profilePage) {
        Element animeStatsDiv = profilePage.getElementsByClass("stats anime").get(0);
        Element headerLine = animeStatsDiv.getElementsByClass("stat-score di-t w100 pt8").get(0);
        Element leftContent = headerLine.getElementsByClass("di-tc al pl8 fs12 fw-b").get(0);
        String content = leftContent.text();
        String[] splittedContent = content.split(" ");
        String daysValue = splittedContent[1];
        return Double.parseDouble(daysValue);
    }

    private double extractMeanScore(Document profilePage) {
        Element animeStatsDiv = profilePage.getElementsByClass("stats anime").get(0);
        Element headerLine = animeStatsDiv.getElementsByClass("stat-score di-t w100 pt8").get(0);
        Element rightContent = headerLine.getElementsByClass("di-tc ar pr8 fs12 fw-b").get(0);
        String content = rightContent.text();
        String[] splittedContent = content.split(" ");
        String meanScoreValue = splittedContent[2];
        return Double.parseDouble(meanScoreValue);
    }

    private int extractWatching(Document profilePage) {
        return extractDetailedAnimeStat(profilePage, ANIME_DETAIL_STATS_INDEX_WATCHING);
    }

    private int extractCompleted(Document profilePage) {
        return extractDetailedAnimeStat(profilePage, ANIME_DETAIL_STATS_INDEX_COMPLETED);
    }

    private int extractOnhold(Document profilePage) {
        return extractDetailedAnimeStat(profilePage, ANIME_DETAIL_STATS_INDEX_ONHOLD);
    }

    private int extractDropped(Document profilePage) {
        return extractDetailedAnimeStat(profilePage, ANIME_DETAIL_STATS_INDEX_DROPPED);
    }

    private int extractPlanToWatch(Document profilePage) {
        return extractDetailedAnimeStat(profilePage, ANIME_DETAIL_STATS_INDEX_PLANTOWATCH);
    }

    private int extractDetailedAnimeStat(Document profilePage, int index) {
        Element animeStatsDiv = profilePage.getElementsByClass("stats anime").get(0);
        Element animeStatsDetails = animeStatsDiv.getElementsByClass("mt12 ml8 mr8 clearfix").get(0);
        Element spanWithCount = animeStatsDetails.getElementsByClass("di-ib fl-r lh10").get(index);
        return Integer.parseInt(spanWithCount.text());
    }
}
