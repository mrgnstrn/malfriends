package com.mrgnstrn.malfriends.scrapping.restclient.entity;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "anime")
public class AnimeData {

    @JacksonXmlProperty(localName = "series_title")
    private String title;
    @JacksonXmlProperty(localName = "series_animedb_id")
    private String id;
    @JacksonXmlProperty(localName = "my_status")
    private String status;
    @JacksonXmlProperty(localName = "series_status")
    private String seriesStatus;
    @JacksonXmlProperty(localName = "series_episodes")
    private int episodes;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSeriesStatus() {
        return seriesStatus;
    }

    public void setSeriesStatus(String seriesStatus) {
        this.seriesStatus = seriesStatus;
    }

    public int getEpisodes() {
        return episodes;
    }

    public void setEpisodes(int episodes) {
        this.episodes = episodes;
    }

    @Override
    public String toString() {
        return "AnimeData{" +
                "title='" + title + '\'' +
                ", id='" + id + '\'' +
                ", status='" + status + '\'' +
                ", seriesStatus='" + seriesStatus + '\'' +
                ", episodes=" + episodes +
                '}';
    }
}
