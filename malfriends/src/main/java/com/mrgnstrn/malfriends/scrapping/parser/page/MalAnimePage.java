package com.mrgnstrn.malfriends.scrapping.parser.page;

public class MalAnimePage {
    private String malId;
    private String title;
    private Double score;
    private Integer usersCountThatRated;
    private Integer ranked;
    private Integer popularity;
    private String type;
    private Integer episodes;
    private String status;
    private String imageUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getUsersCountThatRated() {
        return usersCountThatRated;
    }

    public void setUsersCountThatRated(Integer usersCountThatRated) {
        this.usersCountThatRated = usersCountThatRated;
    }

    public Integer getRanked() {
        return ranked;
    }

    public void setRanked(Integer ranked) {
        this.ranked = ranked;
    }

    public Integer getPopularity() {
        return popularity;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getEpisodes() {
        return episodes;
    }

    public void setEpisodes(Integer episodes) {
        this.episodes = episodes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMalId() {
        return malId;
    }

    public void setMalId(String malId) {
        this.malId = malId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "MalAnimePage{" +
                "malId='" + malId + '\'' +
                ", title='" + title + '\'' +
                ", score=" + score +
                ", usersCountThatRated=" + usersCountThatRated +
                ", ranked=" + ranked +
                ", popularity=" + popularity +
                ", type='" + type + '\'' +
                ", episodes=" + episodes +
                ", status='" + status + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
