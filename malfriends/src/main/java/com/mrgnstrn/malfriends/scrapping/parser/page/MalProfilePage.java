package com.mrgnstrn.malfriends.scrapping.parser.page;

public class MalProfilePage {
    private String username;
    private String avatarUrl;
    private double days;
    private double meansScore;
    private int watching;
    private int completed;
    private int onHold;
    private int dropped;
    private int planToWatch;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public double getDays() {
        return days;
    }

    public void setDays(double days) {
        this.days = days;
    }

    public double getMeansScore() {
        return meansScore;
    }

    public void setMeansScore(double meansScore) {
        this.meansScore = meansScore;
    }

    public int getWatching() {
        return watching;
    }

    public void setWatching(int watching) {
        this.watching = watching;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public int getOnHold() {
        return onHold;
    }

    public void setOnHold(int onHold) {
        this.onHold = onHold;
    }

    public int getDropped() {
        return dropped;
    }

    public void setDropped(int dropped) {
        this.dropped = dropped;
    }

    public int getPlanToWatch() {
        return planToWatch;
    }

    public void setPlanToWatch(int planToWatch) {
        this.planToWatch = planToWatch;
    }

    @Override
    public String toString() {
        return "MalProfilePage{" +
                "username='" + username + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", days=" + days +
                ", meansScore=" + meansScore +
                ", watching=" + watching +
                ", completed=" + completed +
                ", onHold=" + onHold +
                ", dropped=" + dropped +
                ", planToWatch=" + planToWatch +
                '}';
    }
}
