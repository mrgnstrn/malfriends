package com.mrgnstrn.malfriends.scrapping.restclient.entity;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "myanimelist")
public class AnimelistData {
    @JacksonXmlProperty(localName = "myinfo")
    private MyInfoData myInfoData;
    @JacksonXmlProperty(localName = "anime")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<AnimeData> anime;

    public List<AnimeData> getAnime() {
        return anime;
    }

    public void setAnime(List<AnimeData> anime) {
        this.anime = anime;
    }

    public MyInfoData getMyInfoData() {
        return myInfoData;
    }

    public void setMyInfoData(MyInfoData myInfoData) {
        this.myInfoData = myInfoData;
    }

    @Override
    public String toString() {
        return "AnimelistData{" +
                "myInfoData=" + myInfoData +
                ", anime=" + anime +
                '}';
    }
}
