package com.mrgnstrn.malfriends.scrapping.restclient.entity;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "anime")
public class AnimeSearchEntry {
    @JacksonXmlProperty(localName = "id")
    private String id;
    @JacksonXmlProperty(localName = "title")
    private String title;
    @JacksonXmlProperty(localName = "english")
    private String englishTitle;
    @JacksonXmlProperty(localName = "episodes")
    private int episodes;
    @JacksonXmlProperty(localName = "score")
    private double score;
    @JacksonXmlProperty(localName = "type")
    private String type;
    @JacksonXmlProperty(localName = "status")
    private String status;
    @JacksonXmlProperty(localName = "start_date")
    private String startDate;
    @JacksonXmlProperty(localName = "end_date")
    private String endDate;
    @JacksonXmlProperty(localName = "image")
    private String imageUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEnglishTitle() {
        return englishTitle;
    }

    public void setEnglishTitle(String englishTitle) {
        this.englishTitle = englishTitle;
    }

    public int getEpisodes() {
        return episodes;
    }

    public void setEpisodes(int episodes) {
        this.episodes = episodes;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "AnimeSearchEntry{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", englishTitle='" + englishTitle + '\'' +
                ", episodes=" + episodes +
                ", score=" + score +
                ", type='" + type + '\'' +
                ", status='" + status + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
