package com.mrgnstrn.malfriends.cronjobs;

import com.mrgnstrn.malfriends.entity.Anime;
import com.mrgnstrn.malfriends.entity.AnimeList;
import com.mrgnstrn.malfriends.entity.AnimeListEntry;
import com.mrgnstrn.malfriends.service.AnimeListService;
import com.mrgnstrn.malfriends.service.AnimeService;
import com.mrgnstrn.malfriends.service.SynchronizationService;
import com.mrgnstrn.malfriends.service.TimeService;
import com.mrgnstrn.malfriends.task.TaskQueue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;


@SpringBootTest
@RunWith(MockitoJUnitRunner.Silent.class)
public class SynchronizeAnimeJobTest {
    private static final String MAL_ID = "123456";
    @Mock
    private AnimeList animeList;
    @Mock
    private AnimeListEntry animeListEntry;
    @Mock
    private Anime anime;
    @Mock
    private AnimeListService animeListService;
    @Mock
    private AnimeService animeService;
    @Mock
    private TimeService timeService;
    @Mock
    private SynchronizationService synchronizationService;
    @Mock
    private TaskQueue taskQueue;
    @InjectMocks
    private SynchronizeAnimeJob synchronizeAnimeJob;


    private LocalDateTime NOW = LocalDateTime.now();
    private LocalDateTime TWO_DAYS_AGO = NOW.minusDays(2);


    @Before
    public void setUp() {
        when(animeListService.getAllAnimeLists()).thenReturn(Collections.singletonList(animeList));
        when(animeList.getAnimeListEntries()).thenReturn(Collections.singletonList(animeListEntry));
        when(animeListEntry.getMalId()).thenReturn(MAL_ID);
        when(animeService.getAnimeByMalId(MAL_ID)).thenReturn(Optional.of(anime));
        when(anime.getLastUpdated()).thenReturn(TWO_DAYS_AGO);
        when(anime.getMalId()).thenReturn(MAL_ID);
        when(timeService.now()).thenReturn(NOW);
    }

    @Test
    public void shouldSynchronizAnime_whenWasLastUpdatedADayAgo() {
        synchronizeAnimeJob.updateAnimeData();

        verify(taskQueue).add(any());
    }

    @Test
    public void shouldNotUpdateAnime_whenWasLastUpdatedLessThanExpiryTime() {
        when(anime.getLastUpdated()).thenReturn(NOW.minusHours(2));

        synchronizeAnimeJob.updateAnimeData();

        verify(taskQueue, never()).add(any());
    }

}